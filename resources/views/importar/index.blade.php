<!DOCTYPE html>
<html lang="en">
<head>
    {!! Html::script('/js/jquery-3.2.1.min.js') !!}
    {!! Html::script('/js/bootstrap.min.js') !!}
    {!! Html::script('/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/datatables/DataTables-1.10.16/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('js/encargo.js') !!}
    {!! Html::style('/css/bootstrap.min.css') !!}
    {!! Html::style('/css/bootstrapaper.min.css') !!}
    {!! Html::style('/datatables/DataTables-1.10.16/css/dataTables.bootstrap.min.css') !!}


    <meta name="csrf-token" content="{{csrf_token()}}" >
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Importar Encargos</title>
</head>
<body>
@yield('content')
</body>
</html>