@extends('importar.index')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Encargos para importar</h4></div>
                    <div class="panel-body">
                        {!! Form::open(['url'=>'','method'=>'POST','class'=>'']) !!}
                        @include('mensajes.validation')
                        <div class="table-responsive">
                            <table id="example" class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Albaran</th>
                                    <th>Destinatario</th>
                                    <th>Direccón</th>
                                    <th>Población</th>
                                    <th>Cp</th>
                                    <th>Provincia</th>
                                    <th>Telefono</th>
                                    <th>Observaciones</th>
                                    <th>Fecha</th>
                                </tr>

                                </thead>
                                <tbody>
                                @foreach($result as $datos)
                                    @foreach($datos as $dato)

                                        <tr>
                                            <td>{{$dato->albaran}}</td>
                                            <td>{{$dato->destinatario}}</td>
                                            <td>{{$dato->direccion}}</td>
                                            <td>{{$dato->poblacion}}</td>
                                            <td>{{$dato->cp}}</td>
                                            <td>{{$dato->provincia}}</td>
                                            <td>{{$dato->telefono}}</td>
                                            <td>{{$dato->observaciones}}</td>
                                            <td>{{$dato->fecha}}</td>
                                        </tr>

                                    @endforeach
                                @endforeach
                                </tbody>


                            </table>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <br>

                </div>

            </div>

        </div>

    </div>
@endsection