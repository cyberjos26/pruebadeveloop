<!DOCTYPE html>
<html lang="en">
<head>
    {!! Html::script('/js/bootstrap.min.js') !!}
    {!! Html::style('/css/bootstrap.min.css') !!}
    {!! Html::style('/css/bootstrapaper.min.css') !!}

<meta name="csrf-token" content="{{csrf_token()}}" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
    <title>Subir Encargos</title>
</head>
<body>
@yield('content')
</body>
</html>