<div class="form-group">
    {!! Form::label('fichero_origen','Fichero Origen') !!}
    {!! Form::file('fichero_origen',['id'=>'fichero_origen','class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('fila_comienzo','Fila Comienzo') !!}
    {!! Form::text('fila_comienzo',null,['id'=>'fila_comienzo','class'=>'form-control','placeholder'=>'Por favor ingrese la fila de comienzo']) !!}
</div>