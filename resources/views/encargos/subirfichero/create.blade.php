@extends('encargos.index')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Registro de encargos</h4></div>
                    <div class="panel-body">
                        {!! Form::open(['url'=>'encargos/subirfichero','method'=>'POST','class'=>'','enctype'=>'multipart/form-data']) !!}
                        @include('mensajes.validation')
                        @include('encargos.subirfichero.partials.fields')
                        {!! Form::submit('Procesar Fichero',['id'=>'procesar_fichero','class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                    <br>

                </div>

            </div>

        </div>
    </div>

    @endsection