$(document).ready(function(){
    $(document).ready(function() {
        $('#example').DataTable({
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "language": {
                "search":"Buscar",
                "lengthMenu": "Ver _MENU_ registros por pagina",
                "zeroRecords": "Registro no encontrado - Intente de nuevo",
                "info": "Cantidad de paginas _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(Filtrar desde _MAX_ registros en total)",
                "paginate": {
                    "first":      "Primera",
                    "last":       "Ultima",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }
        });
    } );
});