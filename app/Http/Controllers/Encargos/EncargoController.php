<?php

namespace App\Http\Controllers\Encargos;

use App\Http\Requests\SubirFicheroRequest;
use App\Models\Encargo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class EncargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('encargos.subirfichero.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** Metodo para subir el fichero al servidor
     * @param SubirFicheroRequest $request
     */
    public function upFichero(SubirFicheroRequest $request)
    {

       $file=$request->file('fichero_origen');
       $nombre=$file->getClientOriginalName();
       Storage::disk('local')->put($nombre, File::get($file));
       return $this->recorrerFichero($nombre);

    }

    public function recorrerFichero($nombre)
    {

        $public_path = public_path();
        $url = $public_path.'/temp/'.$nombre;

        if (Storage::exists($nombre))
        {

            $culantro=Excel::load($url, function($reader)
            {

              /*foreach ($reader->get() as $datos)
                {
                    /*foreach ($datos as $dato)
                    {
                            $arreglo=array([
                            'albaran'       => $dato->albaran,
                            'destinatario'  => $dato->destinatario,
                            'direccion'     => $dato->direccion,
                            'poblacion'     => $dato->poblacion,
                            'cp'            => $dato->cp,
                            'provincia'     => $dato->provincia,
                            'telefono'      => $dato->telefono,
                            'observaciones' => $dato->observaciones,
                            'fecha'         => $dato->fecha

                        ]);*/

                         /*Encargo::create([
                        'albaran' => $dato->albaran,
                        'destinatario' => $dato->destinatario,
                        'direccion' => $dato->direccion,
                        'poblacion' => $dato->poblacion,
                        'cp' => $dato->cp,
                        'provincia' => $dato->provincia,
                        'telefono' => $dato->telefono,
                        'observaciones' => $dato->observaciones,
                        'fecha' => $dato->fecha

                    ]);
                    }




                } */




            })->get();

            $result=$culantro;


        }


        else
        {
            abort(404);
        }
    return view('importar.importarfichero.create',compact('result'));
    }

    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
