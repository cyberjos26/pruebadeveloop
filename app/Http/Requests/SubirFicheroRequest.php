<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubirFicheroRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fichero_origen' => 'required|mimes:xls,xlsx'

        ];
    }

    public function messages()
    {
        return [
            'fichero_origen.required' => 'No ha seleccionado algun fichero para subir',
            'fichero_origen.mimes'    => 'El fichero a subir no es valido, debe elegir un excel con formato valido: xls,xlsx'

        ];
    }
}
