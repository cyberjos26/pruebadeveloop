<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Encargo
 */
class Encargo extends Model
{
    protected $table = 'encargos';

    public $timestamps = true;

    protected $fillable = [
        'albaran',
        'destinatario',
        'direccion',
        'poblacion',
        'cp',
        'provincia',
        'telefono',
        'observaciones',
        'fecha'
    ];

    protected $guarded = ['id'];

        
}